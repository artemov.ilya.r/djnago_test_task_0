- ### [Задание](docs/task.md)
- ### [Документация API](docs/api.md)
- ### [Структура БД](docs/db.md)

hosted: https://mysterious-ocean-05680.herokuapp.com/

Креды для админки (она без статики)
```
email='admin3@admin.dev'
password='admin'
```