# Авторизация

**URL** : `/auth/`

**Method** : `POST`

**Auth required** : No

#### POST DATA:
```json
{
	"password": str,
	"email": str,
	"username": str,
	"role": 1|2
}
```

Роли:
 - 1 - автор статей
 - 2 - подписчик

## Success Response

**Code** : `201 CREATED`

---

# Просмотр статей

**URL** : `/articles/`

**Method** : `GET`

**Auth required** : No

## Success Response

**Code** : `200 OK`

```json
[
	{
		"text": str,
		"for_sub_only": bool,
		"id": int
	},
    ...
]
```
Для авторизованных пользователей с ролью *подписчик* доступны статьи с ```for_sub_only=True``` 

---

# Создание статьей

**URL** : `/articles/`

**Method** : `POST`

**Auth required** : Yes

**Permissions required** : ```role=1```

#### POST DATA:

```json
{
    "text": str,
    "for_sub_only": bool,
}
```
## Success Response

**Code** : `201 CREATED`

---

# Редактирование статьей

**URL** : `/articles/pk:int/`

**Method** : `PUT`

**Auth required** : Yes

**Permissions required** : ```role=1```, редактировать можно только свои статьи

#### PUT DATA:

```json
{
    "text": str,
    "for_sub_only": bool,
}
```
## Success Response

**Code** : `200 OK`

---

# Удаление статьей

**URL** : `/articles/pk:int/`

**Method** : `DELETE`

**Auth required** : Yes

**Permissions required** : ```role=1```, удалять можно только свои статьи

## Success Response

**Code** : `204`

---