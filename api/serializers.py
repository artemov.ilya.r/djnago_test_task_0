from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework.exceptions import ValidationError, PermissionDenied

from api.models import Account, Article


class SignupSerializer(serializers.ModelSerializer):
    role = serializers.ChoiceField(choices=Account.USER_ROLES)

    class Meta:
        model = Account
        fields = ['email', 'password', 'username', 'role']
        extra_kwargs = {'password': {'write_only': True}}

    def validate_password(self, value):
        validate_password(value)
        return value

    def create(self, validated_data):
        user = get_user_model()(**validated_data)

        user.set_password(validated_data['password'])
        user.save()

        return user


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ['text', 'for_sub_only', 'id']

    def check_permissions(self):
        user = self.context['request'].user
        if user.is_authenticated:
            if user.role == Account.Roles.author:
                return True

        raise PermissionDenied('Wrong permission')

    def create(self, validated_data):
        self.check_permissions()
        author = self.context['request'].user
        return Article.objects.create(
            author=author,
            **validated_data
        )

    def update(self, instance, validated_data):
        self.check_permissions()
        if instance.author != self.context['request'].user:
            raise PermissionDenied('Can edit only your own articles.')
        instance.text = validated_data.get('text', instance.text)
        instance.save()
        return instance
