from django.db import models
from django.contrib.auth.models import AbstractUser

from NewsAPI import settings


class Account(AbstractUser):
    class Roles:
        author = 1
        subscriber = 2
        no_auth = 0

    ordering = ('email',)

    USER_ROLES = (
        (0, 'no-auth'),
        (1, 'author'),
        (2, 'subscriber'),
    )

    role = models.IntegerField(choices=USER_ROLES, blank=True)

    email = models.EmailField(('email address'), unique=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

class Article(models.Model):
    text = models.TextField()
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    for_sub_only = models.BooleanField(default=False)
