from django.contrib import admin
#
# # Register your models here.
from api.models import Article, Account
from django.contrib.auth.admin import UserAdmin


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['id']


admin.site.register(Account, UserAdmin)
