from django.contrib.auth import get_user_model
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.authentication import BasicAuthentication
from rest_framework.exceptions import ValidationError, PermissionDenied
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from api.models import Article, Account
from api.serializers import SignupSerializer, ArticleSerializer


class SignUpView(CreateAPIView):
    model = get_user_model()
    permission_classes = [permissions.AllowAny]
    serializer_class = SignupSerializer


class ArticleView(viewsets.ModelViewSet):
    model = Article

    authentication_classes = [BasicAuthentication]
    permission_classes = [permissions.AllowAny]
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def list(self, request, **kwargs):
        if request.user.is_authenticated:
            if request.user.role == Account.Roles.author:
                queryset = Article.objects.filter(author=request.user).all()
            elif request.user.role == Account.Roles.subscriber:
                queryset = Article.objects.filter().all()
            else:
                queryset = Article.objects.filter(for_sub_only=False).all()
        else:
            queryset = Article.objects.filter(for_sub_only=False).all()

        serializer = ArticleSerializer(queryset, many=True)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.author != self.request.user:
            raise PermissionDenied('You are allowed to delete only your own articles')
        return super().destroy(request, args, kwargs)

    # def update(self, request, *args, **kwargs):
    #     super(ArticleView, self).update(request, args, kwargs)