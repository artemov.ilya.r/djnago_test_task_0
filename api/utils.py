import string

from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

from api.models import Account


class HasUpperCaseValidator:
    def validate(self, password, user=None):
        for char in password:
            if char in string.ascii_uppercase:
                return True
        raise ValidationError(
            _('Your password must contain at least 1 uppercase character.'),
            code='password_has_no_uppercase_character',
        )

    def get_help_text(self):
        return _('Your password must contain at least 1 uppercase character.')
