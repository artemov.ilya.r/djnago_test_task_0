from django.urls import include, path
from rest_framework import routers

from api import views

router = routers.DefaultRouter()

router.register(r'article', views.ArticleView, basename='article')
import pprint
pprint.pprint(router.get_urls())
app_name = 'api'
urlpatterns = [
    path('', include((router.urls, 'api')), name='api'),
    path('auth', views.SignUpView.as_view(), name='signup'),
]