import random
import string

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from api.models import Account, Article


class ApiTest(APITestCase):
    test_accounts = {
        'author_1': {
            'email': 'author@author.com',
            'password': 'Aauthorauthor',
            'username': 'author_1',
            'role': 1
        },
        'author_2': {
            'email': 'author2@author.com',
            'password': 'qweQWEqwe',
            'username': 'author_2',
            'role': 1
        },
        'sub': {
            'email': 'sub@sub.com',
            'password': 'Ssubsubsub',
            'username': 'sub',
            'role': 2
        },
    }
    test_accounts_test_craete = {
        'author_1': {
            'email': 'author1@author.com',
            'password': 'Aauthorauthor',
            'username': 'author_11',
            'role': 1
        },
        'author_2': {
            'email': 'author21@author.com',
            'password': 'qweQWEqwe',
            'username': 'author_21',
            'role': 1
        },
        'sub': {
            'email': 'sub1@sub.com',
            'password': 'Ssubsubsub',
            'username': 'sub1',
            'role': 2
        },
    }

    def setUp(self):
        self.authors = []
        self.subscribers = []
        for user in self.test_accounts.values():
            user_obj = self.__create_user(user)
            if user_obj.role == 1:
                self.authors.append(user_obj)
                for i in range(20):
                    self.__create_article(user_obj)
            elif user_obj.role == 2:
                self.subscribers.append(user_obj)

    def test_delete_articles_authors(self):
        articles = Article.objects.all()

        for author in self.authors:
            author_article = articles.filter(author=author).first()
            author_article_id = author_article.id

            other_author_article = articles.exclude(author=author).first()
            other_author_article_id = other_author_article.id

            self.client.force_authenticate(user=author)

            url = reverse('api:api:article-detail', kwargs={'pk': author_article_id})
            response = self.client.delete(url)

            self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
            self.assertRaises(Article.DoesNotExist, author_article.refresh_from_db)

            url = reverse('api:api:article-detail', kwargs={'pk': other_author_article_id})
            response = self.client.delete(url, format='json')

            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            self.assertEqual(
                Article.objects.get(pk=other_author_article_id),
                other_author_article
            )

    def test_delete_articles_sub(self):
        url = reverse('api:api:article-detail', kwargs={'pk': Article.objects.first().pk})
        self.client.force_authenticate(user=self.subscribers[0])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_articles_anon(self):
        url = reverse('api:api:article-detail', kwargs={'pk': Article.objects.first().pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_edit_articles_authors(self):
        articles = Article.objects.all()

        for author in self.authors:
            for article in articles:
                self.client.force_authenticate(user=author)
                post_data = {
                    'text': self.__get_random_text(),
                    'for_sub_only': random.choice([True, False])
                }
                url = reverse('api:api:article-detail', kwargs={'pk': article.pk})
                response = self.client.put(url, post_data, format='json')
                if article.author == author:
                    self.assertEqual(response.status_code, status.HTTP_200_OK)
                    self.assertEqual(
                        Article.objects.get(pk=article.id).text,
                        post_data['text']
                    )
                else:
                    self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_edit_articles_subs(self):
        articles = Article.objects.all()

        for sub in self.subscribers:
            for article in articles:
                self.client.force_authenticate(user=sub)
                post_data = {
                    'text': self.__get_random_text(),
                    'for_sub_only': random.choice([True, False])
                }
                url = reverse('api:api:article-detail', kwargs={'pk': article.pk})
                response = self.client.put(url, post_data, format='json')
                self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_edit_articles_anon(self):
        articles = Article.objects.all()

        for article in articles:
            post_data = {
                'text': self.__get_random_text(),
                'for_sub_only': random.choice([True, False])
            }
            url = reverse('api:api:article-detail', kwargs={'pk': article.pk})
            response = self.client.put(url, post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_articles_anon(self):
        url = reverse('api:api:article-list')
        response = self.client.get(
            url,
            format='json',
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for article in response.data:
            self.assertEqual(article.get('for_sub_only', None), False)

        self.assertEqual(
            len(response.data),
            Article.objects.filter(for_sub_only=False).count()
        )

    def test_get_articles_subs(self):
        url = reverse('api:api:article-list')
        for sub in self.subscribers:
            self.client.force_authenticate(user=sub)
            response = self.client.get(
                url,
                format='json',
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            self.assertEqual(
                len(response.data),
                Article.objects.filter().count()
            )

    def test_create_articles_authors(self):
        url = reverse('api:api:article-list')

        for author in self.authors:
            for i in range(10):
                data = {
                    'text': self.__get_random_text(),
                    'for_sub_only': random.choice([True, False])
                }
                self.client.force_authenticate(user=author)
                response = self.client.post(
                    url,
                    data,
                    format='json',
                )
                self.assertEqual(response.status_code, status.HTTP_201_CREATED)
                self.assertEqual(
                    Article.objects.filter(text=data['text']).first().text, data['text']
                )

    def test_create_articles_subs(self):
        url = reverse('api:api:article-list')

        for subscriber in self.subscribers:
            data = {
                'text': self.__get_random_text(),
                'for_sub_only': random.choice([True, False])
            }
            self.client.force_authenticate(user=subscriber)
            response = self.client.post(
                url,
                data,
                format='json',
            )
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_articles_anon(self):
        url = reverse('api:api:article-list')

        data = {
            'text': self.__get_random_text(),
            'for_sub_only': random.choice([True, False])
        }
        response = self.client.post(
            url,
            data,
            format='json',
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_accounts(self):
        url = reverse('api:signup')
        for user, data in self.test_accounts_test_craete.items():
            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(Account.objects.filter(email=data['email']).first().email, data['email'])

        for user, data in self.test_accounts.items():
            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def __get_random_text(self):
        return ''.join([
            random.choice(string.ascii_lowercase) for i in range(100)
        ])

    def __create_user(self, data):
        obj = Account.objects.create(
            **data
        )
        obj.save()
        return obj

    def __create_article(self, author):
        Article.objects.create(
            text=self.__get_random_text(),
            author=author,
            for_sub_only=random.choice([True, False])
        )
